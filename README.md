# Demo Online Shop - Client

## Requirements:

For run you need: [Server](https://gitlab.com/ArtemB12/demo-online-shop-server)

## For run:

1) Run your server (go to readme.md and follow instructions)

2) Then:

```
npm i
npm start
```
