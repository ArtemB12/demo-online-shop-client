import { observer } from 'mobx-react-lite';
import React, { useContext, useState } from 'react';
import { Container, Form, Card, Button } from 'react-bootstrap';
import { NavLink, useLocation, useNavigate } from 'react-router-dom';
import { Context } from '../index';
import { login, registration } from '../http/userAPI';
import { LOGIN_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE } from '../utils/consts';

const Auth = observer(() => {
    const {user} = useContext(Context)
    const location = useLocation();
    const navigate = useNavigate();
    const isLogin = location.pathname === LOGIN_ROUTE;
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const click = async () => {
        try {
            let data;
            if (isLogin) {
                data = await login(email, password)
            } else {
                data = await registration(email, password)
                // console.log(response)
            }
            user.setUser(data)
            user.setIsAuth(true) 
            navigate(SHOP_ROUTE)   
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    return (
        <Container 
            className='d-flex justify-content-center align-items-center'
            style={{height: window.innerHeight - 54}}
        >
            <Card style={{width: 600}} className="p-5">
                <h2 className='m-auto'>{isLogin ? 'Authorization' : 'Registration'}</h2>
                <Form className='d-flex flex-column'>
                    <Form.Control 
                        className='mt-2'
                        placeholder='Enter your email...'
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    <Form.Control 
                        className='mt-2'
                        placeholder='Enter your password...'
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type='password'
                    />
                    <Form className='d-flex justify-content-between mt-2 pr-2 pl-2'>
                        {isLogin ? 
                            <div className='mt-1'>
                                Not signed yet? <NavLink to={REGISTRATION_ROUTE} style={{textDecoration:'none'}}>Sign up!</NavLink>
                            </div>
                        :
                            <div>
                                Already registred? <NavLink to={LOGIN_ROUTE} style={{textDecoration:'none'}}>Enter!</NavLink>
                            </div>
                        }
                        <Button 
                            variant='outline-secondary'
                            onClick={click}
                        >
                            {isLogin ? 'Enter' : 'Registration'}
                        </Button>
                    </Form>
                </Form>
            </Card>
        </Container>
    );
});

export default Auth;