import React, { useState, useEffect } from "react";
import { Col, Container, Image, Form, Row, Card, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import bigStar from "../assets/star.png";
import { fetchOneDevice } from "../http/deviceAPI";

const DevicePage = () => {
  const [device, setDevice] = useState({info: []})
  const {id} = useParams()

  useEffect(() => {
    fetchOneDevice(id).then(data => setDevice(data))
  }, [])

  return (
    <Container className="mt-3">
      <Row>
        <Col md={4} className='p-0'>
          <Image width={300} height={300} src={process.env.REACT_APP_API_URL + '/' + device.img} />
        </Col>
        <Col md={4}>
          <Form className="d-flex flex-column align-items-center">
            <h2>{device.name}</h2>
            <div
              className="d-flex justify-content-center align-items-center"
              style={{
                background: `url(${bigStar}) no-repeat center center`,
                width: 240,
                height: 240,
                backgroundSize: "cover",
                fontSize: 64,
              }}
            >
              {device.rating}
            </div>
          </Form>
        </Col>
        <Col md={4}>
            <Card
                className="d-flex flex-column align-items-center justify-content-around"
                style={{width: 300, height: 300, fontSize: 32, border: '5px solid lightgrey'}}
            >
                <h3>From: {device.price} $</h3>
                <Button variant={'outline-dark'}>Put in basket</Button>
            </Card>
        </Col>
      </Row>
      <Row className='d-flex flex-column mt-3'>
        <h1>Specification</h1>
        {device.info.map((info, index) => 
            <Row key={info.id} style={{background: index % 2 === 0 ? 'lightgrey' : 'transparent', padding: 5}}>
                {info.title}: {info.description}
            </Row> 
        )}
      </Row>
    </Container>
  );
};

export default DevicePage;
