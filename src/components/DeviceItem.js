import React, { useContext } from "react";
import { Col, Card } from 'react-bootstrap';
import Image from 'react-bootstrap/Image';
import star from '../assets/star.png';
import { useNavigate } from "react-router-dom";
import { DEVICE_ROUTE } from "../utils/consts";
import { Context } from "../index";

const DeviceItem = ({item}) => {
    const navigate = useNavigate();
    console.log(item.brandId)
    const {device} = useContext(Context);
    console.log(device.brands)
    

    return (
        <Col md={3} className={'mt-3'} onClick={() => navigate(DEVICE_ROUTE + '/' + item.id)}>
            <Card style={{width: 150, cursor: 'pointer'}} border={'light'}>
                <Image width={150} heigth={150} src={process.env.REACT_APP_API_URL + '/' + item.img}/>
                <div className="text-black-50 mt-1 d-flex justify-content-between align-items-center">
                    <div>{device.brands.map(brand => {
                         if (brand.id === item.brandId) {
                            return brand.name
                         }
                        })}</div>
                    <div className="d-flex align-items-center">
                        <div>{item.rating}</div>
                        <Image width={15} heigth={15} src={star}/>
                    </div>
                </div>
                <div>{item.name}</div>
            </Card>
        </Col>
    )
}

export default DeviceItem;