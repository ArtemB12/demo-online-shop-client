import { observer } from 'mobx-react-lite';
import { React, useContext } from 'react';
import { Context } from '../index';
import { Form } from 'react-bootstrap';
import DeviceItem from './DeviceItem';

const DeviceList = observer(() => {
    const {device} = useContext(Context);

    return (
        <Form className='d-flex flex-wrap'>
            {device.devices.map(item => 
                <DeviceItem key={item.id} item={item}/>
            )}
        </Form>
    )
})

export default DeviceList;